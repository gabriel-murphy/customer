import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaximusClaimsComponent } from './maximus-claims.component';

describe('MaximusClaimsComponent', () => {
  let component: MaximusClaimsComponent;
  let fixture: ComponentFixture<MaximusClaimsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaximusClaimsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaximusClaimsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
