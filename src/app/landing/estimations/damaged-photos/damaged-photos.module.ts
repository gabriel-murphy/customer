import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DamagedPhotosRoutingModule } from './damaged-photos-routing.module';
import { DamagedPhotosComponent } from './damaged-photos.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';


@NgModule({
  declarations: [DamagedPhotosComponent],
  imports: [
    CommonModule,
    DamagedPhotosRoutingModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class DamagedPhotosModule { }
