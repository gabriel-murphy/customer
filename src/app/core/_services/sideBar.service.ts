import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SideBarService {

  private activeSubject = 1;
  public active =  new BehaviorSubject(this.activeSubject);

  constructor( ) { }

  setActive(value){
    this.active.next(value);
  }


}
