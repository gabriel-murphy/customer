import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobDetailRoutingModule } from './job-detail-routing.module';
import { JobDetailComponent } from './job-detail.component';
import { CarouselModule } from 'ngx-owl-carousel-o';


@NgModule({
  declarations: [JobDetailComponent],
  imports: [
    CommonModule,
    JobDetailRoutingModule,
    CarouselModule
  ]
})
export class JobDetailModule { }
