import { Component, OnInit ,ElementRef} from '@angular/core';
import {AuthService} from '../../core/_services/auth.service';
import { PhoneCodes } from 'src/app/core/_models/phoneCodes';
import { ToastrService } from 'ngx-toastr';
import { CommonModule } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  email:string;
  my_search_text:string;
  login_code_sent=false;
  isNot:any=isNaN;
  phone_code:any=new PhoneCodes().getCodes();
  countryCode:string='+1'
  login_code:string;

  input_is_focused: false;
  show_colored:false;
  loader:boolean=false;
  is_email:boolean=true;
  focused:boolean=false;
  main_image = "assets/images/login/bg-login-blended.jpg";
  params:any;

  constructor(public authService: AuthService,
              private router:Router,
              private elementRef : ElementRef,
              private toastr: ToastrService,
              private route: ActivatedRoute,
    ) { }

  async ngOnInit() {
    this.loader = true;
    await this.route.queryParams.subscribe(async params => {
      console.log('i am running');
      this.params = params;
      console.log(this.params);
      // this.authService.logoutOlder();
      this.authService.authenticateUser(this.params).subscribe(data=>{
        console.log('data',data);
        this.loader =false;
        this.router.navigate(['/customer/dashboard']);
      },error=>{
        console.log(error);
        this.loader =false;
      });
    });
  }
  onFocus(){
    this.focused = true;
    this.main_image = "assets/images/login/bg-login.jpg";
  }
  onBlur(){
    console.log(this.elementRef.nativeElement.querySelector('#login_email').value.length);
    if(this.elementRef.nativeElement.querySelector('#login_email').value.length==0){
      this.focused = false;
      this.main_image = "assets/images/login/bg-login-blended.jpg";
    }

  }
  sendLoginCode(){
    this.loader = true;

    //check if search text is email or phone if phone attach country code as well

    if(this.ValidateNumber(this.my_search_text) && this.my_search_text.length>10){
      this.toastr.error('Phone number should contain exactly 10 characters', 'Error');
      this.loader = false;
      return;

    }

    let query_param = {'search_text':(this.ValidateNumber(this.my_search_text))?this.countryCode+this.my_search_text:this.my_search_text};

    
    this.authService.sendLoginCode(query_param).subscribe(data=>{
      this.toastr.success('Login Code Sent Successfully!', 'Success');
      this.login_code_sent=true;
      this.loader =false;
    },error=>{
      this.toastr.error(error, 'Error');
      console.log('this is error',error);
      this.loader =false;
    })
    
    
  }

  verifyLoginCode(){
    
    if(this.ValidateNumber(this.my_search_text) && this.my_search_text.length>10){
      this.toastr.error('Phone number should contain exactly 10 characters', 'Error');
      this.loader = false;
      return;

    }
    this.loader = true;
    let query_param = {'search_text':(this.ValidateNumber(this.my_search_text))?this.countryCode+this.my_search_text:this.my_search_text,'login_code':this.login_code};

   
    this.authService.verifyLoginCode(query_param).subscribe(data=>{
      this.toastr.success('Welcome !'+data.first_name+' '+data.last_name, 'Success');
      // console.log('data from API',data);
      this.loader = false;
      this.router.navigate(['/customer/dashboard']);

    },error=>{
      console.log('this is error',error);
      this.loader =false;
      this.toastr.error(error, 'Error');
    })
    
   
  }
  ValidateEmail(input) {

    let validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  
    if (input.match(validRegex)) {
  
        return true;
  
    }
    return false; 
  
  }  
  ValidateNumber(input) {

    let reg = /^\d+$/;
  
    if (input.match(reg)) {
  
        console.log('matched ph');
        return true;
  
    }
      return false; 
  
  }
  
}
