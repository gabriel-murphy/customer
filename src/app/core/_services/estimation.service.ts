import { Injectable } from '@angular/core';
import { HttpHeaders} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { APIService } from './api.service';

@Injectable({
  providedIn: 'root'
})


export class EstimationService {

  constructor(private API: APIService) { }

  // private baseURL = environment.API_END_URL;
  // private headers = new HttpHeaders({
  //   'Content-Type': 'application/json'
  // });

  // private getHeaders() {
  //   const token = localStorage.getItem('token');
  //   // const headers = this.headers.append('Authorization', 'Bearer '+ token);
  //   return headers;
  // }


  getDamagedPhotos(data){
    return this.API.post('esign/getDamagedPhotos',data );
  }
  openEstimate(data){
    return this.API.post('esign/openEstimate',data );
  }
  openAob(data){
    return this.API.post('claim/openClaim',data );
  }
  getEstimate(data){
    return this.API.post('esign/getEstimate',data );
  }
  getAob(data){
    return this.API.post('claim/getClaim',data );
  }
  estimatePrinted(data){
    return this.API.post('esign/estimatePrinted',data );
  }
  sendVerficationPin(data){
    return this.API.post('esign/sendVerificationPin',data );
  }

  resendVerficationPin(data){
    return this.API.post('esign/resendVerificationPin',data );
  }
  verifyVerificationPin(data){

    const headers = new HttpHeaders({
      'mimeType': 'multipart/form-data'
      });

    headers.append('Accept', 'application/json');
    return this.API.post('esign/verifyVerificationPin',data ,headers);
  }

  claimPrinted(data){
    return this.API.post('claim/claimPrinted',data );
  }
  sendVerficationPinforClaim(data){
    return this.API.post('claim/sendVerificationPin',data );
  }

  resendVerficationPinforClaim(data){
    return this.API.post('claim/resendVerificationPin',data );
  }
  verifyVerificationPinforClaim(data){

    const headers = new HttpHeaders({
      'mimeType': 'multipart/form-data'
      });

    headers.append('Accept', 'application/json');
    return this.API.post('claim/verifyVerificationPin',data ,headers);
  }


}
