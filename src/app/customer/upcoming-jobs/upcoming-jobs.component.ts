import { Component, OnInit } from '@angular/core';
import { JobsService } from 'src/app/core/_services/user/jobs.service';
import { JobDetailService } from 'src/app/core/_services/jobdetail.service';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-upcoming-jobs',
  templateUrl: './upcoming-jobs.component.html',
  styleUrls: ['./upcoming-jobs.component.scss']
})
export class UpcomingJobsComponent implements OnInit {
  structures:any;
  loader:boolean=false;
  structure_id:any;
  property_id:any;
  gate_code:any;
  loader2:boolean=false;
  disable_previous:boolean=true;
  disable_next:boolean=false;
  p: number = 1;
  limit: number = 5;
  constructor(
    private jobsService:JobsService,
    private jobDetailService:JobDetailService,
    public router: Router,
    private toastr: ToastrService,
    ) { }

  ngOnInit(): void {
    this.loader = true;
    let offset = (this.p - 1) * this.limit;
    this.jobsService.getUpcomingJobs(offset, this.limit).subscribe(data=>{
      console.log(data);
      this.structures = data.data;
      if(this.structures.length ==0 || this.structures.length <this.limit){
        this.disable_next = true;
      }
      this.loader= false;
      // console.log(this.projects[0].project_milestones);
      // this.projects.forEach((project,ind) => {
      //   project.project_milestones.forEach(milestone => {
      //     console.log(milestone);
      //     project.milestones.push()
      //   });

      // });
    },error=>{
      console.log('error',error);
      this.loader= false;
      this.toastr.error("Something Wrong", 'Error');

    })
  }
  addGateCode(){
    this.loader2 = true;
    this.jobsService.addGateCode({'gate_code':this.gate_code ,'property_id':this.property_id }).subscribe(data=>{
      console.log(data);
      this.structures.forEach((structure,ind) => {
          if(structure.property.id = this.property_id){
            this.structures[ind].property.gate_code = this.gate_code;
          }
      });

      document.getElementById('modal_id').click();
      this.loader2 = false;
      this.toastr.success("Gate Code added Successfully", 'Success');
    },error=>{
      console.log('error',error);
      this.loader2 = false;
      this.toastr.error("Something Wrong", 'Error');

    })
  }
  jobDetail(i){
    let detail = {
      project:this.structures[i].property.project,
      property:this.structures[i].property,
      structure:this.structures[i]
    }
    this.jobDetailService.setJob(detail);
    this.router.navigate(['/customer/job-detail']);

  }

  getPaginatedResults(preference){
    if(preference==0){
      this.p--;
      this.disable_next = false;
    }
    else if(preference==1){
      this.p++;
      this.disable_previous = false;
    }
    this.loader = true;
    
    let offset = (this.p - 1) * this.limit;
    this.jobsService.getUpcomingJobs(offset, this.limit).subscribe(data=>{
      this.structures = data.data;
      if(preference==0 && offset==0){
        if(this.structures.length >= this.limit){
          this.disable_previous = true;
        }
      }
      if(preference==1){
        if(this.structures.length ==0 || this.structures.length <this.limit){
          this.disable_next = true;
        }
      }
      
      this.loader= false;
    },error=>{
      this.loader= false;
      this.toastr.error("Something Wrong", 'Error');

    })
  }

}
