import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompletedJobsRoutingModule } from './completed-jobs-routing.module';
import { CompletedJobsComponent } from './completed-jobs.component';


@NgModule({
  declarations: [CompletedJobsComponent],
  imports: [
    CommonModule,
    CompletedJobsRoutingModule
  ]
})
export class CompletedJobsModule { }
