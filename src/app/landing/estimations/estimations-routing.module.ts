import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EstimationsComponent } from './estimations.component';

const routes: Routes = [{ path: '', component: EstimationsComponent, children:[

  { path: 'damaged-photos', loadChildren: () => import('./damaged-photos/damaged-photos.module').then(m => m.DamagedPhotosModule) },
  { path: 'open-estimation', loadChildren: () => import('./open-estimation/open-estimation.module').then(m => m.OpenEstimationModule) },
  { path: 'estimation-detail', loadChildren: () => import('./estimation-detail/estimation-detail.module').then(m => m.EstimationDetailModule) },
  { path: 'aob-detail', loadChildren: () => import('./aob-detail/aob-detail.module').then(m => m.AobDetailModule) },
  { path: 'signature-confirmed', loadChildren: () => import('./signature-confirmed/signature-confirmed.module').then(m => m.SignatureConfirmedModule) },
  

] },

 

];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstimationsRoutingModule { }
