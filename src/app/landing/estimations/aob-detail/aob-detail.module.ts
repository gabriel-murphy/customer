import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AobDetailRoutingModule } from './aob-detail-routing.module';
import { AobDetailComponent } from './aob-detail.component';

import { SignaturePadModule } from 'angular2-signaturepad';
import { FormsModule } from '@angular/forms';
import { DnddirectivesharedModule } from '../../dnddirectiveshared/dnddirectiveshared.module';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { PdfViewerModule } from 'ng2-pdf-viewer';


@NgModule({
  declarations: [AobDetailComponent],
  imports: [
    CommonModule,
    AobDetailRoutingModule,
    PdfViewerModule,
    SignaturePadModule,
    FormsModule,
    DnddirectivesharedModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    })
  ]
})
export class AobDetailModule { }
