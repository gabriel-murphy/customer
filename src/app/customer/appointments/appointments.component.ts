import { Component, OnInit } from '@angular/core';
import {AppointmentService} from '../../core/_services/user/appointment.service';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
  styleUrls: ['./appointments.component.scss']
})
export class AppointmentsComponent implements OnInit {

  show_active = 1;
  properties:any;
  current_date:any;
  data:any;
  appointments:any;
  loader:boolean=false;
  disable_previous:boolean=true;
  disable_next:boolean=false;
  p: number = 1;
  limit: number = 10;

  constructor(
            private appointmentService:AppointmentService,
            private toastr: ToastrService,
    ) { }

  ngOnInit(): void {
    this.loader = true;
    let offset = (this.p - 1) * this.limit;
    this.appointmentService.getAppointments(offset,this.limit).subscribe(data=>{
      this.data = data.data;
      this.appointments = this.data;
      this.current_date = moment(new Date()).format('yyyy-MM-DD HH:mm:ss');
      if(this.appointments.length ==0 || this.appointments.length <this.limit){
        this.disable_next = true;
      }

      console.log(data);
      this.loader = false;
    },error=>{
      console.log('error',error);
      this.toastr.error("Something Wrong", 'Error');
      this.loader = false;
    })
  }
  getPaginatedResults(preference){
    if(preference==0){
      this.p--;
      this.disable_next = false;
    }
    else if(preference==1){
      this.p++;
      this.disable_previous = false;
    }
    this.loader = true;
    
    let offset = (this.p - 1) * this.limit;

    this.appointmentService.getAppointments(offset,this.limit).subscribe(data=>{
      this.data = data.data;
      this.appointments = this.data;
      if(preference==0 && offset==0){
        if(this.appointments.length >= this.limit){
          this.disable_previous = true;
        }
      }
      if(preference==1){
        if(this.appointments.length ==0 || this.appointments.length <this.limit){
          this.disable_next = true;
        }
      }
      this.current_date = moment(new Date()).format('yyyy-MM-DD HH:mm:ss');

      console.log(data);
      this.loader = false;
    },error=>{
      console.log('error',error);
      this.toastr.error("Something Wrong", 'Error');
      this.loader = false;
    })
  }

  showActiveChanged(value: any): any{
    this.show_active = value;
    this.appointments = [];
    console.log(this.data);
    switch (value) {
      case 1:
          this.appointments= this.data;
          break;
      case 2:
        this.appointments = this.data.filter(data=>{
          return data.appointment_type == 'evaluation' || data.appointment_type == 'followup';
        });
        break;
      case 3:
        this.appointments = this.data.filter(data=>{
          return data.appointment_type == 'dry_in_crew_scheduled';
        });
        break;
      case 4:
        this.appointments = this.data.filter(data=>{
          return data.appointment_type == 'final_crew_scheduled';
        });
        break;
      default:
        this.appointments= [];
        break;
    }

    console.log(this.appointments);

  }
}
