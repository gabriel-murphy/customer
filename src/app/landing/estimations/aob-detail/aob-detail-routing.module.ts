import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AobDetailComponent } from './aob-detail.component';

const routes: Routes = [{ path: '', component: AobDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AobDetailRoutingModule { }
