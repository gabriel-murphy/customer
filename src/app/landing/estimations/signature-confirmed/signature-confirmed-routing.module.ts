import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignatureConfirmedComponent } from './signature-confirmed.component';

const routes: Routes = [{ path: '', component: SignatureConfirmedComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignatureConfirmedRoutingModule { }
