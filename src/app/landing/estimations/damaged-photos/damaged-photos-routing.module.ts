import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DamagedPhotosComponent } from './damaged-photos.component';

const routes: Routes = [{ path: '', component: DamagedPhotosComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DamagedPhotosRoutingModule { }
