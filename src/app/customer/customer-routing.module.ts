import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerComponent } from './customer.component';
import { AuthLoggedInGuard } from '../core/_helpers/auth_loggedin.guard';

const routes: Routes = [{ path: '', component: CustomerComponent, children: [
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule) ,  canActivate:[AuthLoggedInGuard] },
  { path: 'upcoming-jobs', loadChildren: () => import('./upcoming-jobs/upcoming-jobs.module').then(m => m.UpcomingJobsModule) ,  canActivate:[AuthLoggedInGuard]},
  { path: 'claims', loadChildren: () => import('./maximus-claims/maximus-claims.module').then(m => m.MaximusClaimsModule),  canActivate:[AuthLoggedInGuard] },
  { path: 'current-jobs', loadChildren: () => import('./current-jobs/current-jobs.module').then(m => m.CurrentJobsModule) ,  canActivate:[AuthLoggedInGuard]},
  { path: 'job-detail', loadChildren: () => import('./job-detail/job-detail.module').then(m => m.JobDetailModule) ,  canActivate:[AuthLoggedInGuard]},
  { path: 'appointments', loadChildren: () => import('./appointments/appointments.module').then(m => m.AppointmentsModule),  canActivate:[AuthLoggedInGuard] },
  { path: 'estimates', loadChildren: () => import('./estimates/estimates.module').then(m => m.EstimatesModule),  canActivate:[AuthLoggedInGuard] },
  { path: 'my-signature', loadChildren: () => import('./my-signature/my-signature.module').then(m => m.MySignatureModule) ,  canActivate:[AuthLoggedInGuard]},
  { path: 'completed-jobs', loadChildren: () => import('./completed-jobs/completed-jobs.module').then(m => m.CompletedJobsModule) ,  canActivate:[AuthLoggedInGuard]},
  { path: 'claim-detail', loadChildren: () => import('./claim-detail/claim-detail.module').then(m => m.ClaimDetailModule),  canActivate:[AuthLoggedInGuard] },
]},
  


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }
