import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AobDetailComponent } from './aob-detail.component';

describe('AobDetailComponent', () => {
  let component: AobDetailComponent;
  let fixture: ComponentFixture<AobDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AobDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AobDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
