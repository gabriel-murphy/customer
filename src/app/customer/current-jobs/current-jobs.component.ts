import { Component, OnInit,ViewChild } from '@angular/core';
import { JobsService } from 'src/app/core/_services/user/jobs.service';
import {Router} from '@angular/router';
import { SlidesOutputData,OwlOptions, CarouselComponent,CarouselModule, } from 'ngx-owl-carousel-o';
import { JobDetailService } from 'src/app/core/_services/jobdetail.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-current-jobs',
  templateUrl: './current-jobs.component.html',
  styleUrls: ['./current-jobs.component.scss']
})
export class CurrentJobsComponent implements OnInit {

  // @ViewChild("owlElement", { static: true }) owlElement: CarouselComponent;
  disable_previous:boolean=true;
  disable_next:boolean=false;
  p: number = 1;
  limit: number = 10;
  customOptions: OwlOptions = {
    // autoWidth: true,
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    nav: true,
    navSpeed: 1000,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {

        items: 1
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
  };

  structures:any;
  loader:boolean=false;
  loader2:boolean=false;
  activeSlides: SlidesOutputData;
  noc:any;
  structure_id:any;
  constructor(private jobsService:JobsService,
              public router: Router,
              private jobDetailService:JobDetailService,
              private toastr: ToastrService,
    ) { }


  ngOnInit(): void {
    this.loader=true;
    let offset = (this.p - 1) * this.limit;
    this.jobsService.getCurrentJobs(offset,this.limit).subscribe(data=>{
      this.loader=false;
      this.structures=data.data;
      if(this.structures.length ==0 || this.structures.length <this.limit){
        this.disable_next = true;
      }
    },error=>{
      this.loader=false;
      this.toastr.error("Something Wrong", 'Error');

    });
  }

  changeOptions() {
    this.customOptions = { ...this.customOptions, loop: false } // this will make the carousel refresh

  }
  getPassedData(data: SlidesOutputData) {
    this.activeSlides = data;

  }
  getData(data: SlidesOutputData) {

  }

  getPaginatedResults(preference){
    if(preference==0){
      this.p--;
      this.disable_next = false;
    }
    else if(preference==1){
      this.p++;
      this.disable_previous = false;
    }
    this.loader = true;
    
    let offset = (this.p - 1) * this.limit;

    this.jobsService.getCurrentJobs(offset, this.limit).subscribe(data=>{
   
      this.loader=false;
      this.structures=data.data;
      if(preference==0 && offset==0){
        if(this.structures.length >= this.limit){
          this.disable_previous = true;
        }
      }
      if(preference==1){
        if(this.structures.length ==0 || this.structures.length <this.limit){
          this.disable_next = true;
        }
      }
    },error=>{
      this.loader=false;
    
      this.toastr.error("Something Wrong", 'Error');

    });
  }

  fileChange(event,structure_id){
    this.loader2=true;
  
    let form_data = new FormData();
    form_data.append('noc',event.target.files[0]);
    form_data.append('structure_id',structure_id);
    this.jobsService.uploadNOC(form_data).subscribe(data=>{
     
      this.structures.forEach((structures,ind) => {
        if(structures.id == structure_id){
          this.structures[ind].notarized_noc = data.data;
        }
      });
      document.getElementById('modal_id').click();
      this.loader2 = false;
      this.toastr.success("NOC uploaded Successfully", 'Success');
    },error=>{
      this.loader=false;
      console.log('error',error);
      this.toastr.error("Something Wrong", 'Error');

    });
  }
  jobDetail(i){
    console.log(i);

    let detail = {
      project:this.structures[i].property.project,
      property:this.structures[i].property,
      structure:this.structures[i]

    }
    this.jobDetailService.setJob(detail);
    this.router.navigate(['/customer/job-detail']);

  }

  showError(f,g){
    // console.log(f,g);
  }





}
