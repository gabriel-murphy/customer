import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MaximusClaimsComponent } from './maximus-claims.component';

const routes: Routes = [{ path: '', component: MaximusClaimsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MaximusClaimsRoutingModule { }
