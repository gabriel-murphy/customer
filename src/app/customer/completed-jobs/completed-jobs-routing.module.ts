import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompletedJobsComponent } from './completed-jobs.component';

const routes: Routes = [{ path: '', component: CompletedJobsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CompletedJobsRoutingModule { }
