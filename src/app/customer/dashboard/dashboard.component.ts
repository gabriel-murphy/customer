import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/core/_models/user';
import { AuthService } from 'src/app/core/_services';
import { DashboardService } from 'src/app/core/_services/user/dashboard.service';
import {Router} from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';
import * as moment from 'moment';
declare var $: any;
import { ToastrService } from 'ngx-toastr';
import { SideBarService } from 'src/app/core/_services/sideBar.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    nav: true,
    navSpeed: 1000,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 1
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
  };

  loader:boolean=false;
  customer:any;
  show_number:false;
  current_date:any;
  // property:any;
  // structure:any;
  // appointment:any;
  // estimator:any;

  user = new User();
  constructor(private dashboardService:DashboardService,
              private authService:AuthService,
              private router:Router,
              private toastr: ToastrService,
              private sideBarService:SideBarService
    ) { }

  async ngOnInit() {

    this.loader = true;
    this.user = this.authService.currentUserValue;
    this.current_date = new Date();
    await this.getDashboardData();


  }
  async onFileDropped(files){
    // console.log(files);
    // if(files.length>1){
    //   this.toaster.error('You can only upload one image','Image Upload');
    // }
    // this.files = files;
    // this.image_name = this.files[0].name;
    // let extension= this.files[0].type;
    // if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    //   this.image_url = await this.readFile(this.files[0]);
    //   this.image_read=true;
    // }
    // else{
    //   this.toaster.error('Upload A valid image','Image Upload');
    // }
  }
  async fileChange(event) {

    // console.log(event);
    // // this.imageChangedEvent = event;
    // // this.addCustomImage();
    // this.files = this.elementRef.nativeElement.querySelector('#fileDropRef').files;
    // this.image_name = this.files[0].name;
    // let extension= this.files[0].type;
    // if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    //   this.image_url = await this.readFile(this.files[0]);
    //   console.log(this.image_url);
    //   this.image_read=true;
  
    // }
    // else{
    //   this.toaster.error('Upload A valid image','Image Upload');
    // }
  
  }
  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('.venobox').venobox();
    });

  }

  getInterest(interest,id){
    let interests = JSON.parse(interest);
    // let key = Object.keys(interests)[id];
    return interests[Object.keys(interests)[id]];

  }
  getDashboardData(){
    return new Promise((resolve,reject)=>{
      this.dashboardService.getDashboardData().subscribe(data=>{
        this.customer = data.data;
        this.current_date = moment(new Date()).format('yyyy-MM-DD HH:mm:ss');
        console.log(this.current_date);
        // this.property = this.project.property;
        // this.structure = this.structure;
        console.log(data.data);
        this.loader= false;
        $(document).ready(function() {
          $('.venobox').venobox();
        });
        // console.log(this.data.last_project);
        resolve;
      },error=>{
        console.log('error',error);
        this.loader= false;
        this.toastr.error("Something Wrong", 'Error');
        reject;
      })

    })
  }
  formatPhoneNumber(phone){


    phone = phone.replace("+1", '');
    var cleaned = ('' + phone).replace(/\D/g, '');
    var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

    if (match) {
        let string = '(' + match[1] + ') ' + match[2] + '-' + match[3];
        return string;
    }
    return null;
  }

  navigate(path,active){
    console.log(path);
    this.router.navigate([path]);
    this.sideBarService.setActive(active);
  }

}
