import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OpenEstimationComponent } from './open-estimation.component';

describe('OpenEstimationComponent', () => {
  let component: OpenEstimationComponent;
  let fixture: ComponentFixture<OpenEstimationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OpenEstimationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenEstimationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
