import { Component, OnInit, AfterViewInit } from '@angular/core';
import {Router} from '@angular/router';
import { JobDetailService } from 'src/app/core/_services/jobdetail.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { SideBarService } from 'src/app/core/_services/sideBar.service';
declare var $: any;

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss']
})
export class JobDetailComponent implements OnInit, AfterViewInit {

  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    nav: true,
    navSpeed: 1000,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      }
    },
  }

  project:any;
  property:any;
  structure:any;

  constructor(public router: Router,
              private jobDetailService:JobDetailService,
              private sideBarService:SideBarService

    ) { }

  ngOnInit() {
    console.log(this.jobDetailService.currentJob , 'current job');
    if(this.jobDetailService.currentJob==undefined){
      return this.router.navigate(['/customer/dashboard']);
    }else{
      this.jobDetailService.currentJob.subscribe(data=>{
        console.log('detail job',data);
        this.project = data.project;
        this.property = data.property;
        this.structure = data.structure;
      },error=>{
        console.log('inside error');
        console.log(error);
      })
    }
  }
  getInterest(interest,id){
    let interests = JSON.parse(interest);
    // let key = Object.keys(interests)[id];
    return interests[Object.keys(interests)[id]];

  }
  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('.venobox').venobox();
    });

  }

  navigate(path,active){
    console.log(path);
    this.router.navigate([path]);
    this.sideBarService.setActive(active);
  }

}
