import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// import { HeaderComponent } from './layout/header/header.component';
import { environment } from '../environments/environment';
import { HttpClientModule,HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonModule } from '@angular/common';
// import { JwtInterceptor, ErrorInterceptor } from './core/_helpers';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';

import { SignaturePadModule } from 'angular2-signaturepad';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { ErrorInterceptor } from './core/_helpers';

// import {SignatureGeneratorModule} from 'ngp-signature-generator';
// import { DndDirective } from './dnd.directive';


// import { ToastrModule } from 'ngx-toastr';

// import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
// import { ANIMATION_TYPES } from 'ng2-loading-spinner';
// import { GooglePlaceModule } from "ngx-google-places-autocomplete";


@NgModule({
  declarations: [
    AppComponent,
    // DndDirective
  ],
  imports: [
    RouterModule,
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot({
      timeOut: 5000,
    }), // ToastrModule added
    // SignatureGeneratorModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    SignaturePadModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
