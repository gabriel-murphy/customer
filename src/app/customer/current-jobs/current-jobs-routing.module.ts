import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CurrentJobsComponent } from './current-jobs.component';

const routes: Routes = [{ path: '', component: CurrentJobsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrentJobsRoutingModule { }
