import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddSignatureComponent } from './add-signature.component';

const routes: Routes = [{ path: '', component: AddSignatureComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddSignatureRoutingModule { }
