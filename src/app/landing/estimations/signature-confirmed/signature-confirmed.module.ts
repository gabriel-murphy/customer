import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignatureConfirmedRoutingModule } from './signature-confirmed-routing.module';
import { SignatureConfirmedComponent } from './signature-confirmed.component';


@NgModule({
  declarations: [SignatureConfirmedComponent],
  imports: [
    CommonModule,
    SignatureConfirmedRoutingModule
  ]
})
export class SignatureConfirmedModule { }
