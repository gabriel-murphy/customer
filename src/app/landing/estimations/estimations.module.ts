import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstimationsRoutingModule } from './estimations-routing.module';
import { EstimationsComponent } from './estimations.component';
import { HeadfootsharedModule } from './../headfootshared/headfootshared.module';



@NgModule({
  declarations: [EstimationsComponent],
  imports: [
    CommonModule,
    EstimationsRoutingModule,HeadfootsharedModule
  ]
})
export class EstimationsModule { }
