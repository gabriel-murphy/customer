import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';
import { SidebarComponent } from '../layout/sidebar/sidebar.component';
import { CustomerHeaderComponent } from '../layout/customer-header/customer-header.component';
import { TeambuttonComponent } from '../layout/teambutton/teambutton.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
@NgModule({
  declarations: [CustomerComponent,SidebarComponent,CustomerHeaderComponent,TeambuttonComponent],
  imports: [
    CommonModule,
    CustomerRoutingModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class CustomerModule { }
