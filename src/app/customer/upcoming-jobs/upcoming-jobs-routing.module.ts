import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UpcomingJobsComponent } from './upcoming-jobs.component';

const routes: Routes = [{ path: '', component: UpcomingJobsComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UpcomingJobsRoutingModule { }
