import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../core/_services/auth.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  is_loggedin:boolean=false;


  constructor(private router: Router,
    public authService: AuthService,

    ) { }

  ngOnInit(): void {
    this.authService.currentUser.subscribe(user=>{
      console.log('user in header',user);
      user == null ? this.is_loggedin=false: this.is_loggedin=true;

    })
  }

}
