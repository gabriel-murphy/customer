import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EstimationDetailRoutingModule } from './estimation-detail-routing.module';
import { EstimationDetailComponent } from './estimation-detail.component';
// import { AddSignatureModule } from '../../add-signature/add-signature.module';
import { SignaturePadModule } from 'angular2-signaturepad';
import { FormsModule } from '@angular/forms';
import { DnddirectivesharedModule } from '../../dnddirectiveshared/dnddirectiveshared.module';
// import { DndDirective } from '../../../dnd.directive';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { PdfViewerModule } from 'ng2-pdf-viewer';


@NgModule({
  declarations: [EstimationDetailComponent],
  imports: [
    PdfViewerModule,
    CommonModule,
    SignaturePadModule,
    // AddSignatureModule,
    FormsModule,
    DnddirectivesharedModule,
    EstimationDetailRoutingModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    })
  ]
})
export class EstimationDetailModule { }
