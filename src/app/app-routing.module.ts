import { NgModule } from '@angular/core';
import { Routes, RouterModule,PreloadAllModules } from '@angular/router';
import { AuthLoggedInGuard } from './core/_helpers/auth_loggedin.guard';

const routes: Routes = [

  { path: '', loadChildren: () => import('./landing/landing.module').then(m => m.LandingModule) },
  { path: 'customer', loadChildren: () => import('./customer/customer.module').then(m => m.CustomerModule) ,  canActivate:[AuthLoggedInGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true, preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
