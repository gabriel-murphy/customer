import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { AuthService } from 'src/app/core/_services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { SideBarService } from 'src/app/core/_services/sideBar.service';
declare var $: any;

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  show_active = 1;
  constructor(public router: Router,
              private authService:AuthService,
              private sideBarService:SideBarService
    ) { }

  ngOnInit() {
      this.sideBarService.active.subscribe(data=>{
        console.log('data in sidebar',data);
        this.show_active = data;
      },error=>{
        console.log('inside error');
        console.log(error);
      })
    }


  showActiveChanged(value: any): any{
    this.show_active = value;
  }
  logout(){
    console.log('inside logout');
    // this.
    this.authService.logout();
  }
  closeSidebarMenu(){
    $('.sidebar').css('width', 0);
    $('.main-wrapper').css('display', 'block');
    $('#close_sidebar_btn').addClass('d-none');
  }
}
