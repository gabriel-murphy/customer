import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BluePrintService } from 'src/app/core/_services/blueprint.service';
import { EsignerService } from 'src/app/core/_services/esigner.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  params:any;
  loader:boolean=false;

  constructor(public router: Router, private route: ActivatedRoute,
              private bluepirntService:BluePrintService,
              private esignerService:EsignerService
    ) { }

  async ngOnInit() {
    this.loader = true;

    await this.route.queryParams.subscribe(async params => {
      console.log(params);
      this.params = params;
      this.bluepirntService.getBluePrintDetails({params:params}).subscribe(data=>{
        console.log('data',data);
        this.esignerService.SetData(data.data);
        this.loader = false;

      },error=>{
        console.log(error);
        this.loader = false;

      });
    });
  }

  async fileChange(event) {

    // console.log(event);
    // // this.imageChangedEvent = event;
    // // this.addCustomImage();
    // this.files = this.elementRef.nativeElement.querySelector('#fileDropRef').files;
    // this.image_name = this.files[0].name;
    // let extension= this.files[0].type;
    // if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    //   this.image_url = await this.readFile(this.files[0]);
    //   console.log(this.image_url);
    //   this.image_read=true;
  
    // }
    // else{
    //   this.toaster.error('Upload A valid image','Image Upload');
    // }
  
  }
  async onFileDropped(files){
    // console.log(files);
    // if(files.length>1){
    //   this.toaster.error('You can only upload one image','Image Upload');
    // }
    // this.files = files;
    // this.image_name = this.files[0].name;
    // let extension= this.files[0].type;
    // if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    //   this.image_url = await this.readFile(this.files[0]);
    //   this.image_read=true;
    // }
    // else{
    //   this.toaster.error('Upload A valid image','Image Upload');
    // }
  }

  async routerNavigator(path:string){
    await this.router.navigate([path],{queryParams:this.params});
  }

}

