import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { User } from 'src/app/core/_models/user';
import { DashboardService } from 'src/app/core/_services/user/dashboard.service';
import {AuthService} from '../../core/_services/auth.service';
declare var $: any;

@Component({
  selector: 'customer-app-header',
  templateUrl: './customer-header.component.html',
  styleUrls: ['./customer-header.component.scss']
})
export class CustomerHeaderComponent implements OnInit {

  is_loggedin:boolean=false;
  user = new User();
  file = new FormData();
  constructor(private router: Router,
              public authService: AuthService,
              private dashboardService:DashboardService

  ) { }

  ngOnInit(): void {
    this.authService.currentUser.subscribe(user=>{
      this.user = user ;
      console.log(user);
    },error=>{
      console.log(error);
    })
  }

  uploadCustomerImage(event){
    console.log(event.target.files[0],'files');
    let form_data = new FormData();
    // this.support_files_array.push(upload);
    form_data.append('image', event.target.files[0], event.target.files[0].name);
    console.log(form_data.get('image'));
    this.authService.uploadCustomerImage(form_data).subscribe(data=>{
      console.log('response',data);
    },error=>{
      console.log('error',error);
    })
  }


  showSidebarMenu(){
    $('.sidebar').css('width', 100+'%');
    $('.main-wrapper').css('display', 'none');
    $('#close_sidebar_btn').removeClass('d-none');
  }




}
