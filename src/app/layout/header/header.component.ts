import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { User } from 'src/app/core/_models/user';
import { EsignerService } from 'src/app/core/_services/esigner.service';
import {AuthService} from '../../core/_services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  is_loggedin:boolean=false;
  user= new User();
  data:any;
  constructor(private router: Router,
    public authService: AuthService,
    private esignerService:EsignerService

    ) { }

  ngOnInit() {
    this.esignerService.data_subject.subscribe(data=>{
      console.log('from header',data);
      this.data = data;
      this.data = data;
    })

    // this.authService.currentUser.subscribe(user=>{
    //   console.log('user in header',user);
    //   user == null ? this.is_loggedin=false: this.is_loggedin=true;

    // })
    this.authService.currentUser.subscribe(user=>{
      this.user = user ;
    },error=>{
      console.log(error);
    })
  }

}
