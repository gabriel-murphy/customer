import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddSignatureRoutingModule } from './add-signature-routing.module';
import { AddSignatureComponent } from './add-signature.component';
import { SignaturePadModule } from 'angular2-signaturepad';


@NgModule({
  declarations: [AddSignatureComponent],
  imports: [
    CommonModule,
    AddSignatureRoutingModule,
    SignaturePadModule

  ]
})
export class AddSignatureModule { }
