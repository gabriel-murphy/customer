import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { CarouselModule } from 'ngx-owl-carousel-o';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    CarouselModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class DashboardModule { }
