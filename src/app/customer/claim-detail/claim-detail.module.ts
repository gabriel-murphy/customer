import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClaimDetailRoutingModule } from './claim-detail-routing.module';
import { ClaimDetailComponent } from './claim-detail.component';


@NgModule({
  declarations: [ClaimDetailComponent],
  imports: [
    CommonModule,
    ClaimDetailRoutingModule
  ]
})
export class ClaimDetailModule { }
