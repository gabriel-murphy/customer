import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SignatureConfirmedComponent } from './signature-confirmed.component';

describe('SignatureConfirmedComponent', () => {
  let component: SignatureConfirmedComponent;
  let fixture: ComponentFixture<SignatureConfirmedComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SignatureConfirmedComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SignatureConfirmedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
