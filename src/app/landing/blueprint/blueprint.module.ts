import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlueprintRoutingModule } from './blueprint-routing.module';
import { BlueprintComponent } from './blueprint.component';
import { HeadfootsharedModule } from './../headfootshared/headfootshared.module';



@NgModule({
  declarations: [BlueprintComponent],
  imports: [
    CommonModule,
    BlueprintRoutingModule,
    HeadfootsharedModule

  ]
})
export class BlueprintModule { }
