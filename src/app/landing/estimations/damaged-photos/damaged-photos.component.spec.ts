import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DamagedPhotosComponent } from './damaged-photos.component';

describe('DamagedPhotosComponent', () => {
  let component: DamagedPhotosComponent;
  let fixture: ComponentFixture<DamagedPhotosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DamagedPhotosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DamagedPhotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
