import { Injectable } from '@angular/core';
import { Router , CanActivate, CanActivateChild, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../_services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanActivateChild, CanLoad {

  constructor(private route: Router,private authenticationService: AuthService)
  {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const currentUser = this.authenticationService.currentUserValue;
        
        if (currentUser) {
            // check if route is restricted by role
            if (route.data.roles && route.data.roles.indexOf(JSON.parse(localStorage.getItem('role'))) === -1) {
                // role not authorised so redirect to home page
                this.route.navigate(['']);
                return false;
            }

            // authorised so return true
            return true;
        }
      // not logged in so redirect to login page with the return url
      this.route.navigate([''], { queryParams: { returnUrl: state.url } });
      return false;
  }

  loggedIn() {
   
    return this.authenticationService.loggedIn();
  }

  canActivateChild(
    
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
    return true;
  }
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
      
    return true;
  }
}
