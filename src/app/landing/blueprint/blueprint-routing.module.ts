import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlueprintComponent } from './blueprint.component';



const routes: Routes = [{ path: '', component: BlueprintComponent, children:[

  { path: 'upload', loadChildren: () => import('./upload/upload.module').then(m => m.UploadModule) },

] },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlueprintRoutingModule { }
