import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MySignatureRoutingModule } from './my-signature-routing.module';
import { MySignatureComponent } from './my-signature.component';


@NgModule({
  declarations: [MySignatureComponent],
  imports: [
    CommonModule,
    MySignatureRoutingModule
  ]
})
export class MySignatureModule { }
