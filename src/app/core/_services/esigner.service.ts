import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EsignerService {

  private data:any = 0;
  public  data_subject = new BehaviorSubject(this.data);

  constructor( ) { }

  SetData(count) {
    this.data_subject.next(count);
  }


}
