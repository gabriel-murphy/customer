export class User{
  public id:number;
  public role:string='customer';
  public first_name:string;
	public last_name:string;
  public image:string;
	public phone:string;
  public email:string;
  public salutation:string;
}
