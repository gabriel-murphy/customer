import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstimationDetailComponent } from './estimation-detail.component';

describe('EstimationDetailComponent', () => {
  let component: EstimationDetailComponent;
  let fixture: ComponentFixture<EstimationDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstimationDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstimationDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
