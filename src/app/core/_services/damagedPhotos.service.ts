import { Injectable } from '@angular/core';
import { APIService } from './api.service';
import { map, shareReplay } from 'rxjs/operators';

import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})


export class DamagedPhotosService {

  // public damaged_photos: Observable<any>;

  constructor(private API: APIService ) {;

   }

   getDamagedPhotos(data){
    return this.API.post('esign/getDamagedPhotos',data );
    // return this.damaged_photos;
  }
  getAOBDamagedPhotos(data){
    return this.API.post('claim/getDamagedPhotos',data );
    // return this.damaged_photos;
  }

  // private baseURL = environment.API_END_URL;
  // private headers = new HttpHeaders({
  //   'Content-Type': 'application/json'
  // });

  // private getHeaders() {
  //   const token = localStorage.getItem('token');
  //   // const headers = this.headers.append('Authorization', 'Bearer '+ token);
  //   return headers;
  // }


  // getDamagedPhotos(data){
  //   return this.API.post('esign/getDamagedPhotos',data );
  // }
  // sendVerficationPin(data){
  //   return this.API.post('esign/sendVerificationPin',data );
  // }

  // resendVerficationPin(data){
  //   return this.API.post('esign/resendVerificationPin',data );
  // }
  // verifyVerficationPin(data){
  //   return this.API.post('esign/verifyVerficationPin',data );
  // }


}
