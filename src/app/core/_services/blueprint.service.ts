import { Injectable } from '@angular/core';
import { APIService } from './api.service';

@Injectable({
  providedIn: 'root'
})


export class BluePrintService {

  constructor(private API: APIService) { }

  getBluePrintDetails(data){
    return this.API.post('blueprints/getBluePrintDetails',data );
  }
  uploadBlueprints(data){
    return this.API.post('blueprints/uploadBlueprints',data );
  }

}
