import { OnInit, AfterViewInit, Component,ViewChild,ElementRef } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad';
import { Router, ActivatedRoute } from '@angular/router';
import { EstimationService } from 'src/app/core/_services/estimation.service';
import { DomSanitizer } from '@angular/platform-browser';
import {Signature} from '../../../core/_models/signature';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { EsignerService } from 'src/app/core/_services/esigner.service';
import { ToastrService } from 'ngx-toastr';
import { UploadService } from 'src/app/core/_services/upload.service';
import {AuthService} from '../../../core/_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { NgZone } from '@angular/core';

declare var window: any;


@Component({
  selector: 'app-estimation-detail',
  templateUrl: './estimation-detail.component.html',
  styleUrls: ['./estimation-detail.component.scss']
})
export class EstimationDetailComponent implements OnInit,AfterViewInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  is_active=true;
  is_loggedin:boolean=false;

  params:any;
  // draw_signature:boolean=true;
  // create_signature:boolean=false;
  // upload_signature:boolean=false;
  signature_type='draw';
  pin_type='email';
  signature_confirmed:boolean=false;
  signature_drawn:boolean=false;
  pin_generated:boolean=false;
  signature = new Signature();
  current_date = new Date();
  structure:any;
  files: any;
  image_name:any;
  image_url:any;
  signature_created:boolean=false;
  image_read:boolean=false;
  font_selected:boolean=false;
  font_img:any;
  pdf:any;
  verified:boolean=false;
  estimate:any;
  loader:boolean=false;
  loader2:boolean=false;
  due_date:any;
  pdfSrc : any =  "";

  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 800,
    'canvasHeight': 244,
  };

  constructor(
    public router             :   Router,
    private route             :   ActivatedRoute,
    private estimationService :   EstimationService,
    private _sanitizer        :   DomSanitizer,
    private elementRef        :   ElementRef,
    private esignerService    :   EsignerService,
    private toaster           :   ToastrService,
    private uploadService     :   UploadService,
    public authService        :   AuthService,
    // public fileSaver          :   FileSaver
    private _httpClient: HttpClient,
    protected sanitizer: DomSanitizer,
    private zone: NgZone

  ) { }

  async ngOnInit() {

    this.loader = true;
    this.authService.currentUser.subscribe(user=>{
      user == null ? this.is_loggedin=false: this.is_loggedin=true;
      if(this.is_loggedin){
        this.signature.full_name = this.titleCase(user.first_name) + " "+ this.titleCase(user.last_name);
      }

    });
    await this.route.queryParams.subscribe(async params => {
      console.log(params);
      this.params=params;
      // this.estimationService.damaged_photos;
    
        this.estimationService.getEstimate({params:params}).subscribe(data=>{
          console.log('data',data);
          this.structure = data.data;
          this.esignerService.SetData(data.data);
          this.pdfSrc = this.structure.estimates[0].unsigned_proposal;
          // this.loader = false;
  
          this.due_date = new Date(this.structure.estimates[0].created_at);
          this.due_date.setDate( this.due_date.getDate() + 7 );
          console.log(this.due_date,'due date');
          // this.convertUrls();
  
        },error=>{
          console.log(error);
          this.loader = false;
        });
    

    });
  }


  getInterest(interest,id){
    let interests = JSON.parse(interest);
    // let key = Object.keys(interests)[id];
    return interests[Object.keys(interests)[id]];

  }
  titleCase(string) {
    let sentence = string.toLowerCase().split(" ");
    for (let i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }

    return sentence.join(" ");
  }
  // convertUrls(){

  //   this.toDataURL(this.structure.property.appointments[0].estimator.signature_image, function (dataUrl) {
  //     if(dataUrl){
  //       (<HTMLImageElement>document.getElementById('signature_img')).src = dataUrl;
  //       (<HTMLImageElement>document.getElementById('signature_img2')).src = dataUrl;
  //     }
  //   });
  // }
  toDataURL(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
      var reader = new FileReader();
      reader.onloadend = function () {
        callback(reader.result);
      }
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
  }
  printPdf() {
    // window.open(this.pdfSrc);
    // this.estimatePrinted();
    this.zone.runOutsideAngular(() => window.print());

    // const pdf = new Blob([this.pdfSrc], { type: 'application/pdf' });
    // const blobUrl = URL.createObjectURL(pdf);
    // const blobUrl = this.pdfSrc;
    // const iframe = document.createElement('iframe');
    // iframe.style.display = 'none';
    // iframe.src = this.pdfSrc;
    // document.body.appendChild(iframe);
    // iframe.contentWindow.print();

    // var doc = new jspdf.jsPDF('p', 'px','letter');
    // this.pdf = doc.output(this.pdfSrc);
    // doc.save( 'tes');
    // this.winRef.open(this.pdfSrc).print();
  }

  callBackFn(evnt){
    console.log(evnt);
    this.loader=false;
  }
  signatureSelectionChanged(id:any){
    // console.log(id);
    // let radio = this.elementRef.nativeElement.querySelector('#'+id);
    // radio.checked=true;
    this.signature_type = id;
    if(id==='draw'){
      this.signature_drawn=false;
      this.signature_confirmed=false;
      // this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    }else if(id==='create'){
      this.signature_created=false;
      this.signature_confirmed=false;
      // this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    }


    // if(id==='draw'){
    //   this.draw_signature=true;
    //   this.create_signature=false;
    //   this.upload_signature=false;
    // }else if(id==='create'){
    //   this.draw_signature=false;
    //   this.create_signature=true;
    //   this.upload_signature=false;
    // }else if(id==='upload'){
    //   this.draw_signature=false;
    //   this.create_signature=false;
    //   this.upload_signature=true;
    // }
  }

  ngAfterViewInit() {
    // this.signaturePad is now available
    this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    // const iframe = (document.getElementById('proposalIframe') as HTMLIFrameElement);
    // iframe.contentWindow.addEventListener('click',() => console.log('click'));

  }

  drawComplete() {
    // document.getElementById('dismiss_modal').click();

    // will be notified of szimek/signature_pad's onEnd event
    this.signature.image = this.signaturePad.toDataURL();
    this.signature_drawn = true;
    // console.log(this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
    //              + this.signaturePad.toDataURL()));
  }

  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }


  async routerNavigator(path:string){
    await this.router.navigate([path],{queryParams:this.params});

  }


  sendVerficationPin(){
    this.loader2 = true;
    this.pin_generated=true;
    this.estimationService.sendVerficationPin({params:this.params, pin_type:this.pin_type}).subscribe(data=>{
      console.log('data',data);
      this.loader2 = false;
      this.toaster.success('Verification Pin Sent Successfully!', 'Success');
    },error=>{
      console.log(error);
      this.loader2 = false;

    });
  }
  resendVerficationPin(){
    // this.loader2 = true;
    this.pin_generated=true;
    this.estimationService.resendVerficationPin({params:this.params, pin_type:this.pin_type}).subscribe(data=>{
      console.log('data',data);
      this.loader2 = false;
      this.toaster.success('Verification Pin Sent Successfully!', 'Success');
    },error=>{
      console.log(error);
      this.loader2 = false;

    });
  }

  async verifyVerficationPin(){
    this.loader2 = true;
    // await this.generatePDF(1);

    // this.uploadService.uploadFile(this.pdf);
    // return;

    let form_data = new FormData();
    // this.support_files_array.push(upload);
    // form_data.append('file', this.pdf, 'pdf.pdf');

    for ( var key in this.signature ) {
      form_data.append(key, this.signature[key]);
      console.log('key'+key);
    }

    form_data.append('params',JSON.stringify(this.params));


    this.estimationService.verifyVerificationPin(form_data).subscribe(data=>{
      console.log('data',data);
      let modal = document.getElementById('dismiss_modal');
      console.log(modal,'modal');
      modal.click();
      this.loader2 = true;
      this.verified = true;
      this.estimate = data.data;
      this.toaster.success('Thanks For Signing The '+this.estimate.product_info.roof_type +' Estimation', 'Success');
      // this.router.navigate(['estimation/signature-confirmed']);
    },error=>{
      console.log(error);
      this.loader2 = false;
      this.toaster.error(error, 'Error');
    });
  }
  /*
  |-------------------------------------------------------|
  |             Generate PDF Function                     |
  |-------------------------------------------------------|
  */
//   generatePDF(method) {
//     return new Promise((resolve,reject)=>{
//       /*     First Page      */
//       let data = document.getElementById('contentToConvert');
//       html2canvas(data, { scale:2, allowTaint: true , scrollX:0, scrollY: -window.scrollY }).then(canvas => {

//         var doc = new jspdf.jsPDF('p', 'px','letter');
//         let imgData = canvas.toDataURL('image/png');
//         doc.addImage(imgData, 'PNG', 0, 0, doc.internal.pageSize.getWidth(), doc.internal.pageSize.getHeight(),'','FAST');
//         doc.getImageProperties(imgData);
//         /*     Second Page      */
//         let data2 = document.getElementById('contentToConvert2');
//         html2canvas(data2, { scale:2, allowTaint: true , scrollX:0, scrollY: -window.scrollY }).then(canvas => {
//           let imgData2 = canvas.toDataURL('image/png');
//           doc.addPage();
//           doc.addImage(imgData2, 'PNG',0, 0, doc.internal.pageSize.getWidth(), doc.internal.pageSize.getHeight(),'','FAST');
//           /*     Third Page      */
//           let data3 = document.getElementById('contentToConvert3');
//           html2canvas(data3, { scale:2, allowTaint: true , scrollX:0, scrollY: -window.scrollY }).then(canvas => {
//             let imgData3 = canvas.toDataURL('image/png');
//             doc.addPage();
//             doc.addImage(imgData3, 'PNG',0, 0, doc.internal.pageSize.getWidth(), doc.internal.pageSize.getHeight(),'','FAST');
//             /*     Fourth Page      */
//             let data4 = document.getElementById('contentToConvert4');
//             html2canvas(data4, { scale:2, allowTaint: true , scrollX:0, scrollY: -window.scrollY }).then(canvas => {
//               let imgData4 = canvas.toDataURL('image/png');
//               doc.addPage();
//               doc.addImage(imgData4, 'PNG',0, 0, doc.internal.pageSize.getWidth(), doc.internal.pageSize.getHeight(),'','FAST');
//               /*     Fifth Page      */
//               let data5 = document.getElementById('contentToConvert5');
//               html2canvas(data5, { scale:2, allowTaint: true , scrollX:0, scrollY: -window.scrollY }).then(canvas => {
//                 let imgData5 = canvas.toDataURL('image/png');
//                 doc.addPage();
//                 doc.addImage(imgData5, 'PNG',0, 0, doc.internal.pageSize.getWidth(), doc.internal.pageSize.getHeight(),'','FAST');

//                 if(!method){
//                   doc.save( 'file.pdf');
//                   this.estimatePrinted();
//                   resolve(1);

//                 }else{
//                   console.log(doc.output('blob'),'doc here');
//                   this.pdf = doc.output('blob');
//                   resolve(this.pdf);
//                 };

//               });

//             });

//           });


//         });
//       });



// // html2canvas(data, { allowTaint: true , scrollX:0, scrollY: -window.scrollY }).then(canvas => {
// //   console.log(canvas.toDataURL('image/png'),'canvas ehre');

// //   var imgData = canvas.toDataURL('image/png');

// //   var doc = new jspdf.jsPDF('p', 'px','a4');
// //   console.log(doc.internal.pageSize.getWidth());
// //   console.log(doc.internal.pageSize.getHeight());
// //   console.log(doc.internal.pageSize.getHeight());
// //   const imgProps= doc.getImageProperties(imgData);
// //   console.log('imgProps',imgProps);
// //   let a = document.createElement('a');
// //   a.href = imgData;
// //   a.download = 'image_name.png';
// //   a.style.display = 'none';
// //   document.body.appendChild(a);
// //   a.click();
// //   a.remove();

// //   // return;
// //   const imgWidth = doc.internal.pageSize.getWidth();
// //   console.log('imgWidth',imgWidth);
// //   const imgHeight = (imgProps.height * imgWidth) / imgProps.width;
// //   var pageHeight = imgHeight/5;
// //   var heightLeft = imgHeight;
// //   var position = 0;


// //   doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight,'','FAST');
// //   heightLeft -= pageHeight;
// //   while (heightLeft >= 0) {
// //     position = heightLeft - imgHeight;
// //     doc.addPage();
// //     doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight,'','FAST');
// //     heightLeft -= pageHeight;
// //   }
// //   if(!method){
// //     doc.save( 'file.pdf');
// //     resolve(1);
// //   }else{
// //     console.log(doc.output('blob'),'doc here');
// //     this.pdf = doc.output('blob');
// //     resolve(this.pdf);
// //   }
// // });
// })

// }
estimatePrinted(){

   this.route.queryParams.subscribe(async params => {
    console.log(params);
    this.params=params;
    // this.estimationService.damaged_photos;
    this.estimationService.estimatePrinted({params:params}).subscribe(data=>{
      console.log('data',data);

    },error=>{
      console.log(error);
    });
  });
}

fontSelectionChanged(font){
  let selected_font = this.elementRef.nativeElement.querySelector('#selected_font');
  selected_font.innerHTML = this.signature.full_name;
  selected_font.style.fontFamily = font;
  console.log(selected_font,'selected_font');
  var data = document.getElementById('selected_font');

  html2canvas(selected_font ,{
    width: 200,
    height:200
  }).then(canvas => {
    console.log('canvas',canvas);
    let contentDataURL = canvas.toDataURL('image/jpg',1.0);
    console.log(contentDataURL);
    this.font_img = contentDataURL;
    this.font_selected = true;
  });
}






/**
 * on file drop handler
 */
// onFileDropped(files) {
//   this.prepareFilesList(files);
// }

/**
 * handle file from browsing
 */
// fileBrowseHandler(files) {
//   this.prepareFilesList(files);
// }

/**
 * Delete file from files list
 * @param index (File index)
 */
deleteFile() {
  this.files = [];
  this.image_read = false;
}

/**
 * Simulate the upload process
 */
// uploadFilesSimulator(index: number) {
//   setTimeout(() => {
//     if (index === this.files.length) {
//       return;
//     } else {
//       const progressInterval = setInterval(() => {
//         if (this.files[index].progress === 100) {
//           clearInterval(progressInterval);
//           this.uploadFilesSimulator(index + 1);
//         } else {
//           this.files[index].progress += 5;
//         }
//       }, 200);
//     }
//   }, 1000);
// }

/**
 * Convert Files list to normal array list
 * @param files (Files List)
 */
async onFileDropped(files){
  console.log(files);
  if(files.length>1){
    this.toaster.error('You can only upload one image','Image Upload');
  }
  this.files = files;
  this.image_name = this.files[0].name;
  let extension= this.files[0].type;
  if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    this.image_url = await this.readFile(this.files[0]);
    this.image_read=true;
  }
  else{
    this.toaster.error('Upload A valid image','Image Upload');
  }
}
async fileChange(event) {

  console.log(event);
  // this.imageChangedEvent = event;
  // this.addCustomImage();
  this.files = this.elementRef.nativeElement.querySelector('#fileDropRef').files;
  this.image_name = this.files[0].name;
  let extension= this.files[0].type;
  if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    this.image_url = await this.readFile(this.files[0]);
    console.log(this.image_url);
    this.image_read=true;

  }
  else{
    this.toaster.error('Upload A valid image','Image Upload');
  }

}
readFile = (file) => {
  const fileReader = new FileReader();
  return new Promise((resolve, reject) => {
    fileReader.onerror = () => {
      fileReader.abort();
      console.log('aborted');
      reject('aborted');
    };
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.readAsDataURL(file);
  });
};
uploadConfirmed(){
  // if(this.image_read){
  this.signature_confirmed=true;
  this.signature.image = this.image_url;
  // }
}
createConfirmed(){
  this.signature_confirmed=true;
  this.signature.image = this.font_img;
}
/**
 * format bytes
 * @param bytes (File size in bytes)
 * @param decimals (Decimals point)
 */
// formatBytes(bytes, decimals) {
//   if (bytes === 0) {
//     return '0 Bytes';
//   }
//   const k = 1024;
//   const dm = decimals <= 0 ? 0 : decimals || 2;
//   const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
//   const i = Math.floor(Math.log(bytes) / Math.log(k));
//   return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
// }

dashboard(){
  this.router.navigate(['/customer/dashboard']);
}
 formatPhoneNumber(phone,country_code){


  phone = phone.replace(country_code, '');
  var cleaned = ('' + phone).replace(/\D/g, '');
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
      let string = '(' + match[1] + ') ' + match[2] + '-' + match[3];
      return string;
  }
  return null;
}
formatCurrentMaterial(current_material){
    let type = '';
    if(current_material==1){
      type = "Shingles";
    }
    else if(current_material==2){
      type = "Tile";
    }
    else if (current_material==3){
      type = "Metal";
    }
    return type;
}
}
