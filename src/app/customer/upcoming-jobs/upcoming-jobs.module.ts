import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UpcomingJobsRoutingModule } from './upcoming-jobs-routing.module';
import { UpcomingJobsComponent } from './upcoming-jobs.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';

@NgModule({
  declarations: [UpcomingJobsComponent],
  imports: [
    CommonModule,
    UpcomingJobsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class UpcomingJobsModule { }
