import { Component, OnInit } from '@angular/core';
import { JobsService } from 'src/app/core/_services/user/jobs.service';
import { JobDetailService } from 'src/app/core/_services/jobdetail.service';

import { ClaimService } from 'src/app/core/_services/user/claim.service';
import { ClaimDetailService } from 'src/app/core/_services/claimdetail.service';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-maximus-claims',
  templateUrl: './maximus-claims.component.html',
  styleUrls: ['./maximus-claims.component.scss']
})

export class MaximusClaimsComponent implements OnInit {
  structures:any;
  loader:boolean=false;
  structure_id:any;
  property_id:any;
  params:any;
  gate_code:any;
  loader2:boolean=false;
  disable_previous:boolean=true;
  disable_next:boolean=false;
  p: number = 1;
  limit: number = 10;
  constructor(
    private jobsService:JobsService,
    private ClaimService:ClaimService,
    private ClaimDetailService:ClaimDetailService,
    private jobDetailService:JobDetailService,
    public router: Router,
    private toastr: ToastrService,
    ) { }

  ngOnInit(): void {
    this.loader = true;
    let offset = (this.p - 1) * this.limit;
    this.ClaimService.getInsuranceClaims(offset,this.limit).subscribe(data=>{
      console.log(data);
      this.structures = data.data;
      if(this.structures.length ==0 || this.structures.length <this.limit){
        this.disable_next = true;
      }
      this.loader= false;
    },error=>{
      console.log('error',error);
      this.loader= false;
      this.toastr.error("Something Wrong", 'Error');

    })
  }
  claimDetail(i){
    console.log('in claim detail'+i);
    let detail = {
      project:this.structures[i].property.project,
      property:this.structures[i].property,
      structure:this.structures[i]
    }
    this.ClaimDetailService.setJob(detail);
    this.router.navigate(['/customer/claim-detail']);

  }
  titleCase(string) {
    let sentence = string.toLowerCase().split("_");
    for (let i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }

    return sentence.join(" ");
  }
  openClaim(structure){
    this.loader = true;
    if(structure.claim.is_signed){
      window.open(structure.claim.signed_aob, '_blank');
    }else{
      this.ClaimService.openClaims({claim_id:structure.claim.id}).subscribe(data=>{
        console.log(data);
        this.params = data.data;
        this.router.navigate(['/estimation/damaged-photos'],{queryParams:{link:this.params.link,customer_id:this.params.customer_id,property_id:this.params.property_id,structure_id:this.params.structure_id,claim_id:this.params.claim_id}});
        // this.properties = data.data;
        this.loader = false;

      },error=>{
        console.log('error',error);
        this.loader = false;
        this.toastr.error("Something Wrong", 'Error');

      })
    }
  }
  getPaginatedResults(preference){
    if(preference==0){
      this.p--;
      this.disable_next = false;
    }
    else if(preference==1){
      this.p++;
      this.disable_previous = false;
    }
    this.loader = true;
    
    let offset = (this.p - 1) * this.limit;

    this.ClaimService.getInsuranceClaims(offset, this.limit).subscribe(data=>{
      console.log(data);
      this.structures = data.data;
      if(preference==0 && offset==0){
        if(this.structures.length >= this.limit){
          this.disable_previous = true;
        }
      }
      if(preference==1){
        if(this.structures.length ==0 || this.structures.length <this.limit){
          this.disable_next = true;
        }
      }
      this.loader= false;
    },error=>{
      console.log('error',error);
      this.loader= false;
      this.toastr.error("Something Wrong", 'Error');

    })

  
  }

}
