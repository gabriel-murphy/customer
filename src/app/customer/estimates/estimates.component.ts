import { Component, OnInit } from '@angular/core';
import { EstimateService } from 'src/app/core/_services/user/estimate.service';
import {Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-estimates',
  templateUrl: './estimates.component.html',
  styleUrls: ['./estimates.component.scss']
})
export class EstimatesComponent implements OnInit {

  show_active = 1;
  properties:any;
  params:any;
  loader:boolean=false;
  disable_previous:boolean=true;
  disable_next:boolean=false;
  p: number = 1;
  limit: number = 10;

  constructor(private estimateService:EstimateService,
              public router: Router,
              private toastr: ToastrService,

      ) { }

  ngOnInit(): void {
    this.loader = true;
    let offset = (this.p - 1) * this.limit;
    this.estimateService.getEstimates(offset,this.limit).subscribe(data=>{


    if(data.data.length==0){
      this.disable_next = true;

    }
    else {
        data.data.forEach(property => {
          // console.log(property,'property');
          property.structures.forEach(structure => {
            structure.estimates.forEach(estimate=>{
              if(structure.estimates[0].is_signed==true && estimate.is_signed==false){
                estimate.disabled = true;
              }else if(estimate.is_signed==10){
                estimate.disabled = true;
              }
              estimate.disabled = false;
            })
          });
        this.properties = data.data;
        if(this.properties.length ==0 || this.properties.length <this.limit){
          this.disable_next = true;
        }

        console.log( 'properties' +this.properties);
      });
    }

    this.loader = false;
    },error=>{
      console.log('error',error);
      this.loader = false;
      this.toastr.error("Something Wrong", 'Error');

    })
  }

  ngAfterViewInit(): void {
    window.history.forward();
    window.onpageshow = function(evt) {
      if (evt.persisted) {window.history.forward()};
    }
  }
  showActiveChanged(value: any): any{
    this.show_active = value;
  }

  openEstimate(estimate){
    this.loader = true;
    if(estimate.is_signed){
      window.open(estimate.signed_proposal, '_blank');
    }else{
      this.estimateService.openEstimates({estimate_id:estimate.id}).subscribe(data=>{
        console.log(data);
        this.params = data.data;
        this.router.navigate(['/estimation/damaged-photos'],{queryParams:{link:this.params.link,customer_id:this.params.customer_id,property_id:this.params.property_id,structure_id:this.params.structure_id,estimate_id:this.params.estimate_id}});
        // this.properties = data.data;
        this.loader = false;

      },error=>{
        console.log('error',error);
        this.loader = false;
        this.toastr.error("Something Wrong", 'Error');

      })
    }
  }
  getPaginatedResults(preference){
    if(preference==0){
      this.p--;
      this.disable_next = false;
    }
    else if(preference==1){
      this.p++;
      this.disable_previous = false;
    }
    this.loader = true;
    
    let offset = (this.p - 1) * this.limit;

    this.estimateService.getEstimates(offset,this.limit).subscribe(data=>{

      data.data.forEach(property => {
          // console.log(property,'property');
          property.structures.forEach(structure => {
            structure.estimates.forEach(estimate=>{
              if(structure.estimates[0].is_signed==true && estimate.is_signed==false){
                estimate.disabled = true;
              }else if(estimate.is_signed==10){
                estimate.disabled = true;
              }
              estimate.disabled = false;
            })
          });
      this.properties = data.data;
      if(preference==0 && offset==0){
        if(this.properties.length >= this.limit){
          this.disable_previous = true;
        }
      }
      if(preference==1){
        if(this.properties.length ==0 || this.properties.length <this.limit){
          this.disable_next = true;
        }
      }
    

      console.log( this.properties);
    });
    this.loader = false;
    },error=>{
      console.log('error',error);
      this.loader = false;
      this.toastr.error("Something Wrong", 'Error');

    })
  }
}
