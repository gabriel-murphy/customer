import { Injectable } from '@angular/core';
import {APIService} from './api.service';
import { BehaviorSubject,Observable, throwError} from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from '../_models/user';
import {Router} from '@angular/router';
import { HttpErrorResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  public userRoleSubject: BehaviorSubject<any>;

  constructor(private API :APIService,
    private router: Router) {
      this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('user')!));
      this.currentUser = this.currentUserSubject.asObservable();
      // this.userRoleSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('role')));
      this.userRoleSubject = new BehaviorSubject<any>(localStorage.getItem('role'));


    // this.userRole = this.userRoleSubject.asObservable();
   }

   public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  public changeCurrentUser(user:User){
    localStorage.setItem('user', JSON.stringify(user));
    this.currentUserSubject.next(user);
  }

  register(user:any) {
    return this.API.post('auth/register', user).pipe(map(data => {
    


      if (data.data.user && data.data.token) {
          localStorage.setItem('token', data.data.token);
          localStorage.setItem('user', JSON.stringify(data.data.user));
          localStorage.setItem('role', data.data.user.role);
          data.data.user.token = data.data.token;
          this.currentUserSubject.next(data.data.user);

      }
      // this.sendMessage(data.user.role);
      return data.data.user;
    }));


  }
  // revokeAcccess(data){
  //   return this.API.post('customer/revokeAccess',data );
  // }

  // revokeOldUserAccess(){

  //   let user = JSON.parse(localStorage.getItem('user'));
  //   this.revokeAcccess({user_id:user.id}).subscribe(data=>{

  //   },error=>{
     

  //   })
  // }

  uploadCustomerImage(data){
    let token = localStorage.getItem('token');

    const headers = new HttpHeaders({
      'mimeType': 'multipart/form-data',
      'Authorization': 'Bearer '+ token
      });
      headers.append('Accept', 'application/json');

    return this.API.post('customer/uploadCustomerImage',data, headers).pipe(map(data => {

      if (data.data) {
          localStorage.setItem('user', JSON.stringify(data.data));

          this.currentUserSubject.next(data.data);
      }
      return data.data;
    }));

  }

  sendLoginCode(data){

    return this.API.post('auth/sendLoginCode',data);
  }
  authenticateUser(params){
    //this.revokeOldUserAccess();
    return this.API.post('auth/authenticateUser',params).pipe(map(data => {
      // login successful if there's a jwt token in the response
      
      if (data.data.token && data.data.user) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          //this.logout();
          this.logoutOlder();
          localStorage.setItem('token', data.data.token);
          localStorage.setItem('user', JSON.stringify(data.data.user));
          localStorage.setItem('role', JSON.stringify(data.data.user.role));

          data.data.user.token = data.data.token;
          this.currentUserSubject.next(data.data.user);
          
          // this.currentUserSubject.next(data.data.user);
      }
      return data.data.user;
     
      // this.sendMessage(data.user.role);
      
      
    }));
  }

  verifyLoginCode(data){
    return this.API.post('auth/verifyLoginCode',data).pipe(map(data => {
      // login successful if there's a jwt token in the response
      if (data.data.token && data.data.user) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('token', data.data.token);
          localStorage.setItem('user', JSON.stringify(data.data.user));
          localStorage.setItem('role', JSON.stringify(data.data.user.role));

          this.currentUserSubject.next(data.data.user);

          data.data.user.token = data.data.token;
          // this.currentUserSubject.next(data.data.user);
      }
      // this.sendMessage(data.user.role);
      return data.data.user;
    }));
  }
  login(user:any) {


		return this.API.post('auth/sendLoginCode', user).pipe(map(data => {
            // login successful if there's a jwt token in the response
            if (data.data.token && data.data.user) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('token', data.data.token);
                localStorage.setItem('user', JSON.stringify(data.data.user));
                localStorage.setItem('role', JSON.stringify(data.data.user.role));

                this.currentUserSubject.next(data.data.user);

                data.data.user.token = data.data.token;
                // this.currentUserSubject.next(data.data.user);
            }
            // this.sendMessage(data.user.role);
            return data.data.user;
        }));

	}


	role(){
		var user=JSON.parse(localStorage.getItem('user')!);
		return user.role;
  }

  logoutFront(){
    localStorage.clear();
    this.currentUserSubject.next(null);
    this.router.navigate(['']);
}
  logoutOlder(){
    localStorage.clear();
    this.currentUserSubject.next(null);
  }

	async logout() {
    let token = localStorage.getItem('token');
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    headers.append('Authorization', 'Bearer '+ token);
    this.API.get('customer/logout',headers).subscribe((data => {
    }),error=>{
      console.log(error);
    });
    // this.logoutFront();

  }

	loggedIn(){
		if(localStorage.getItem('token'))
		{

			return true;
    }

		return false;
	}



}
