import { Injectable } from '@angular/core';
import { HttpHeaders} from '@angular/common/http';
import { APIService } from '../api.service';


@Injectable({
  providedIn: 'root'
})


export class AppointmentService {

  constructor(private API: APIService) { }

  private headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });

  private getHeaders() {
    const token = localStorage.getItem('token');
    const headers = this.headers.append('Authorization', 'Bearer '+ token);
    return headers;
  }

  getAppointments(offset: number, limit: number){
    return this.API.get('customer/getAppointments?offset='+offset+'&limit='+limit,this.getHeaders());
  }


}
