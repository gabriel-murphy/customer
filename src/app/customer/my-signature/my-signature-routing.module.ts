import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MySignatureComponent } from './my-signature.component';

const routes: Routes = [{ path: '', component: MySignatureComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MySignatureRoutingModule { }
