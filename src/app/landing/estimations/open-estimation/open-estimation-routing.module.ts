import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OpenEstimationComponent } from './open-estimation.component';

const routes: Routes = [{ path: '', component: OpenEstimationComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OpenEstimationRoutingModule { }
