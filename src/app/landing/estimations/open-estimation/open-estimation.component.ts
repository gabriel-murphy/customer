import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EsignerService } from 'src/app/core/_services/esigner.service';
import { EstimationService } from 'src/app/core/_services/estimation.service';

@Component({
  selector: 'app-open-estimation',
  templateUrl: './open-estimation.component.html',
  styleUrls: ['./open-estimation.component.scss']
})
export class OpenEstimationComponent implements OnInit {
  params:any;
  loader:boolean=false;
  data:any;

  constructor(public router: Router, private route: ActivatedRoute,
              private estimationService:EstimationService,
              private esignerService:EsignerService
    ) { }

  async ngOnInit() {
    this.loader = true;

    await this.route.queryParams.subscribe(async params => {
      console.log(params);
      this.params = params;
      if(this.params.estimate_id){
        this.estimationService.openEstimate({params:params}).subscribe(data=>{
          console.log('data',data);
          this.esignerService.SetData(data.data);
          this.data = data.data;
          this.loader = false;
  
        },error=>{
          console.log(error);
          this.loader = false;
  
        });
      }
      else if(this.params.claim_id){

        this.estimationService.openAob({params:params}).subscribe(data=>{
          console.log('data',data);
          this.esignerService.SetData(data.data);
          this.data = data.data;
          this.loader = false;
  
        },error=>{
          console.log(error);
          this.loader = false;
  
        });
      }

    });
  }

  async routerNavigator(path:string){
    await this.router.navigate([path],{queryParams:this.params});
  }

}
