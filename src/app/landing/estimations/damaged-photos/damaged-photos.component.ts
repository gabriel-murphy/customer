import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EsignerService } from 'src/app/core/_services/esigner.service';
import { EstimationService } from 'src/app/core/_services/estimation.service';
import { DamagedPhotosService } from 'src/app/core/_services/damagedPhotos.service';

declare var $: any;

@Component({
  selector: 'app-damaged-photos',
  templateUrl: './damaged-photos.component.html',
  styleUrls: ['./damaged-photos.component.scss']
})
export class DamagedPhotosComponent implements OnInit {

  damaged_photos:any;
  params:any;
  loader:boolean=false;
  constructor(public router: Router, private route: ActivatedRoute,
              private estimationService:EstimationService,
              private esignerService:EsignerService,
              private damagedPhotosService:DamagedPhotosService
    ) { }


  async ngOnInit() {
    this.loader = true;
    await this.route.queryParams.subscribe(async params => {
      console.log('i am running');
      this.params = params;
      console.log(this.params);

      if(this.params.estimate_id){
        this.damaged_photos = this.damagedPhotosService.getDamagedPhotos({params:params}).subscribe(data=>{
          console.log('data',data);
          this.damaged_photos = data.data.structure_media;
          this.esignerService.SetData(data.data);
          console.log(this.damaged_photos);
          $(document).ready(function() {
            $('.venobox').venobox();
          });
          this.loader = false;
        },error=>{
          this.loader = false;
          console.log(error);
        });
      }
      else if(this.params.claim_id) {

        this.damaged_photos = this.damagedPhotosService.getAOBDamagedPhotos({params:params}).subscribe(data=>{
          console.log('data',data);
          this.damaged_photos = data.data.structure_media;
          this.esignerService.SetData(data.data);
          console.log(this.damaged_photos);
          $(document).ready(function() {
            $('.venobox').venobox();
          });
          this.loader = false;
        },error=>{
          this.loader = false;
          console.log(error);
        });
      }

    });
  }

  async routerNavigator(path:string){
    await this.router.navigate([path],{queryParams:this.params});

  }

  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('.venobox').venobox();
    });

  }

}
