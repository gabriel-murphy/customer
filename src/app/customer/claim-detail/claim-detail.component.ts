import { Component, OnInit, AfterViewInit } from '@angular/core';
import {Router} from '@angular/router';
import { ClaimDetailService } from 'src/app/core/_services/claimdetail.service';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { SideBarService } from 'src/app/core/_services/sideBar.service';
declare var $: any;

@Component({
  selector: 'app-claim-detail',
  templateUrl: './claim-detail.component.html',
  styleUrls: ['./claim-detail.component.scss']
})

export class ClaimDetailComponent implements OnInit, AfterViewInit {

  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    nav: true,
    navSpeed: 1000,
    navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 2
      },
      940: {
        items: 3
      }
    },
  }

  project:any;
  property:any;
  structure:any;

  constructor(public router: Router,
              private ClaimDetailService:ClaimDetailService,
              private sideBarService:SideBarService

    ) { }

  ngOnInit() {
    if(this.ClaimDetailService.currentJob==undefined){
      return this.router.navigate(['/customer/dashboard']);
    }else{
      this.ClaimDetailService.currentJob.subscribe(data=>{
        this.project = data.project;
        this.property = data.property;
        this.structure = data.structure;
      },error=>{
      })
    }
  }
  titleCase(string) {
    let sentence = string.toLowerCase().split("_");
    for (let i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }

    return sentence.join(" ");
  }
  ngAfterViewInit(): void {
    $(document).ready(function() {
      $('.venobox').venobox();
    });

  }

  navigate(path,active){
    this.router.navigate([path]);
    this.sideBarService.setActive(active);
  }

}
