import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EstimationDetailComponent } from './estimation-detail.component';

const routes: Routes = [{ path: '', component: EstimationDetailComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EstimationDetailRoutingModule { }
