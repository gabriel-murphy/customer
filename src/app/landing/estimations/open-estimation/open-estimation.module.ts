import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OpenEstimationRoutingModule } from './open-estimation-routing.module';
import { OpenEstimationComponent } from './open-estimation.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';


@NgModule({
  declarations: [OpenEstimationComponent],
  imports: [
    CommonModule,
    OpenEstimationRoutingModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class OpenEstimationModule { }
