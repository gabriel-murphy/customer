import { Injectable } from '@angular/core';
import { BehaviorSubject,Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobDetailService {

  private jobSubject: BehaviorSubject<any>;
  public currentJob: Observable<any> ;

  constructor( ) { }

  setJob(job){
    this.jobSubject = new BehaviorSubject(job);
    this.currentJob = this.jobSubject.asObservable();
  }


}
