import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {FormsModule} from '@angular/forms';
import { LandingComponent } from './landing.component';
import { AuthNotLoggedInGuard } from '../core/_helpers/authnot_loggedin.guard';


const routes: Routes = [
  { path: '', component: LandingComponent , children: [
    { path: '', loadChildren: () => import('./login/login.module').then(m => m.LoginModule),canActivate:[AuthNotLoggedInGuard] },
    { path: 'login', loadChildren: () => import('./login/login.module').then(m => m.LoginModule) ,canActivate:[AuthNotLoggedInGuard]},
    // { path: 'register', loadChildren: () => import('./register/register.module').then(m => m.RegisterModule) },
    { path: 'estimation', loadChildren: () => import('./estimations/estimations.module').then(m => m.EstimationsModule) },
    { path: 'add-signature', loadChildren: () => import('./add-signature/add-signature.module').then(m => m.AddSignatureModule) },
    { path: 'blueprints', loadChildren: () => import('./blueprint/blueprint.module').then(m => m.BlueprintModule) },
  ]},


];



@NgModule({
  imports: [RouterModule.forChild(routes) ,
    FormsModule
  ],
  exports: [RouterModule]
})
export class LandingRoutingModule { }
