import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaximusClaimsRoutingModule } from './maximus-claims-routing.module';
import { MaximusClaimsComponent } from './maximus-claims.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { FormsModule ,ReactiveFormsModule}   from '@angular/forms';


@NgModule({
  declarations: [MaximusClaimsComponent],
  imports: [
    CommonModule,
    MaximusClaimsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class MaximusClaimsModule { }
