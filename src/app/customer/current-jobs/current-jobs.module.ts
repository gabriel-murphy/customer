import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CurrentJobsRoutingModule } from './current-jobs-routing.module';
import { CurrentJobsComponent } from './current-jobs.component';
import { ANIMATION_TYPES } from 'ng2-loading-spinner';
import { Ng2LoadingSpinnerModule } from 'ng2-loading-spinner';
import { CarouselModule } from 'ngx-owl-carousel-o';


@NgModule({
  declarations: [CurrentJobsComponent],
  imports: [
    CommonModule,
    CurrentJobsRoutingModule,
    CarouselModule,
    Ng2LoadingSpinnerModule.forRoot({
      animationType  : ANIMATION_TYPES.fadingCircle,
      backdropColor  : 'rgba(0, 0, 0, 0.3)',
      spinnerColor   : '#fff'
    }),
  ]
})
export class CurrentJobsModule { }
