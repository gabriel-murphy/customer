import { OnInit, AfterViewInit, Component,ViewChild,ElementRef } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad';
import { Router, ActivatedRoute } from '@angular/router';
import { EstimationService } from 'src/app/core/_services/estimation.service';
import { DomSanitizer } from '@angular/platform-browser';
import {Signature} from '../../../core/_models/signature';
import * as jspdf from 'jspdf';
import html2canvas from 'html2canvas';
import { EsignerService } from 'src/app/core/_services/esigner.service';
import { ToastrService } from 'ngx-toastr';
import { UploadService } from 'src/app/core/_services/upload.service';
import {AuthService} from '../../../core/_services/auth.service';
import { HttpClient } from '@angular/common/http';
import { NgZone } from '@angular/core';

declare var window: any;

@Component({
  selector: 'app-aob-detail',
  templateUrl: './aob-detail.component.html',
  styleUrls: ['./aob-detail.component.scss']
})

export class AobDetailComponent implements OnInit,AfterViewInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  is_active=true;
  is_loggedin:boolean=false;

  params:any;
  signature_type='draw';
  pin_type='email';
  signature_confirmed:boolean=false;
  signature_drawn:boolean=false;
  pin_generated:boolean=false;
  signature = new Signature();
  current_date = new Date();
  structure:any;
  files: any;
  image_name:any;
  image_url:any;
  signature_created:boolean=false;
  image_read:boolean=false;
  font_selected:boolean=false;
  font_img:any;
  pdf:any;
  verified:boolean=false;
  claim:any;
  loader:boolean=false;
  loader2:boolean=false;
  due_date:any;
  pdfSrc : any =  "";

  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 800,
    'canvasHeight': 244,
  };

  constructor(
    public router             :   Router,
    private route             :   ActivatedRoute,
    private estimationService :   EstimationService,
    private _sanitizer        :   DomSanitizer,
    private elementRef        :   ElementRef,
    private esignerService    :   EsignerService,
    private toaster           :   ToastrService,
    private uploadService     :   UploadService,
    public authService        :   AuthService,
    // public fileSaver          :   FileSaver
    private _httpClient: HttpClient,
    protected sanitizer: DomSanitizer,
    private zone: NgZone

  ) { }

  async ngOnInit() {

    this.loader = true;
    this.authService.currentUser.subscribe(user=>{
      user == null ? this.is_loggedin=false: this.is_loggedin=true;
    
      if(this.is_loggedin){
        
        this.signature.full_name = this.titleCase(user.first_name) + " "+ this.titleCase(user.last_name);
      }

    });
    await this.route.queryParams.subscribe(async params => {
      console.log(params);
      this.params=params;
      this.estimationService.getAob({params:params}).subscribe(data=>{
        console.log('data',data);
        this.structure = data.data;
        this.pdfSrc = this.structure.claim.unsigned_aob;
        this.signature.full_name = this.titleCase(this.structure.property.customer.first_name) + " "+ this.titleCase(this.structure.property.customer.last_name);
        this.esignerService.SetData(data.data);
        // this.loader = false;

        this.due_date = new Date(this.structure.claim.created_at);
        this.due_date.setDate( this.due_date.getDate() + 7 );

      },error=>{
        console.log(error);
        this.loader = false;
      });
    

    });
  }


  getInterest(interest,id){
    let interests = JSON.parse(interest);
    // let key = Object.keys(interests)[id];
    return interests[Object.keys(interests)[id]];

  }
  titleCase(string) {
    let sentence = string.toLowerCase().split(" ");
    for (let i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }

    return sentence.join(" ");
  }


  printPdf() {
    this.zone.runOutsideAngular(() => window.print());

  }

  callBackFn(evnt){
    console.log(evnt);
    this.loader=false;
  }
  signatureSelectionChanged(id:any){

    this.signature_type = id;
    if(id==='draw'){
      this.signature_drawn=false;
      this.signature_confirmed=false;
      // this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    }else if(id==='create'){
      this.signature_created=false;
      this.signature_confirmed=false;
      // this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
    }

  }

  ngAfterViewInit() {
    // this.signaturePad is now available
    this.signaturePad.set('minWidth', 2); // set szimek/signature_pad options at runtime
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API


  }

  drawComplete() {

    this.signature.image = this.signaturePad.toDataURL();
    this.signature_drawn = true;

  }

  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }


  async routerNavigator(path:string){
    await this.router.navigate([path],{queryParams:this.params});

  }


  sendVerficationPin(){
    this.loader2 = true;
    this.pin_generated=true;
    this.estimationService.sendVerficationPinforClaim({params:this.params, pin_type:this.pin_type}).subscribe(data=>{
      console.log('data',data);
      this.loader2 = false;
      this.toaster.success('Verification Pin Sent Successfully!', 'Success');
    },error=>{
      console.log(error);
      this.loader2 = false;

    });
  }
  resendVerficationPin(){
    // this.loader2 = true;
    this.pin_generated=true;
    this.estimationService.resendVerficationPinforClaim({params:this.params, pin_type:this.pin_type}).subscribe(data=>{
      console.log('data',data);
      this.loader2 = false;
      this.toaster.success('Verification Pin Sent Successfully!', 'Success');
    },error=>{
      console.log(error);
      this.loader2 = false;

    });
  }

  async verifyVerficationPin(){
    this.loader2 = true;

    let form_data = new FormData();


    for ( var key in this.signature ) {
      form_data.append(key, this.signature[key]);
      console.log('key'+key);
    }

    form_data.append('params',JSON.stringify(this.params));


    this.estimationService.verifyVerificationPinforClaim(form_data).subscribe(data=>{
      console.log('data',data);
      let modal = document.getElementById('dismiss_modal');
      console.log(modal,'modal');
      modal.click();
      this.loader2 = true;
      this.verified = true;
      this.claim = data.data;
      this.toaster.success('Thanks For Signing The AOB', 'Success');
      // this.router.navigate(['estimation/signature-confirmed']);
    },error=>{
      console.log(error);
      this.loader2 = false;
      this.toaster.error(error, 'Error');
    });
  }

estimatePrinted(){

   this.route.queryParams.subscribe(async params => {
    console.log(params);
    this.params=params;
    // this.estimationService.damaged_photos;
    this.estimationService.claimPrinted({params:params}).subscribe(data=>{
      console.log('data',data);

    },error=>{
      console.log(error);
    });
  });
}

fontSelectionChanged(font){
  let selected_font = this.elementRef.nativeElement.querySelector('#selected_font');
  selected_font.innerHTML = this.signature.full_name;
  selected_font.style.fontFamily = font;
  console.log(selected_font,'selected_font');
  var data = document.getElementById('selected_font');

  html2canvas(selected_font ,{
    width: 200,
    height:200
  }).then(canvas => {
    console.log('canvas',canvas);
    let contentDataURL = canvas.toDataURL('image/jpg',1.0);
    console.log(contentDataURL);
    this.font_img = contentDataURL;
    this.font_selected = true;
  });
}


deleteFile() {
  this.files = [];
  this.image_read = false;
}

async onFileDropped(files){
  console.log(files);
  if(files.length>1){
    this.toaster.error('You can only upload one image','Image Upload');
  }
  this.files = files;
  this.image_name = this.files[0].name;
  let extension= this.files[0].type;
  if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    this.image_url = await this.readFile(this.files[0]);
    this.image_read=true;
  }
  else{
    this.toaster.error('Upload A valid image','Image Upload');
  }
}
async fileChange(event) {

  this.files = this.elementRef.nativeElement.querySelector('#fileDropRef').files;
  this.image_name = this.files[0].name;
  let extension= this.files[0].type;
  if(extension=='image/jpeg' || extension=='image/png' || extension=='image/jpp' || extension=='image/svg'){
    this.image_url = await this.readFile(this.files[0]);
    console.log(this.image_url);
    this.image_read=true;

  }
  else{
    this.toaster.error('Upload A valid image','Image Upload');
  }

}
readFile = (file) => {
  const fileReader = new FileReader();
  return new Promise((resolve, reject) => {
    fileReader.onerror = () => {
      fileReader.abort();
      console.log('aborted');
      reject('aborted');
    };
    fileReader.onload = () => {
      resolve(fileReader.result);
    };
    fileReader.readAsDataURL(file);
  });
};
uploadConfirmed(){
  // if(this.image_read){
  this.signature_confirmed=true;
  this.signature.image = this.image_url;
  // }
}
createConfirmed(){
  this.signature_confirmed=true;
  this.signature.image = this.font_img;
}

dashboard(){
  this.router.navigate(['/customer/dashboard']);
}
 formatPhoneNumber(phone,country_code){


  phone = phone.replace(country_code, '');
  var cleaned = ('' + phone).replace(/\D/g, '');
  var match = cleaned.match(/^(\d{3})(\d{3})(\d{4})$/);

  if (match) {
      let string = '(' + match[1] + ') ' + match[2] + '-' + match[3];
      return string;
  }
  return null;
}
formatCurrentMaterial(current_material){
    let type = '';
    if(current_material==1){
      type = "Shingles";
    }
    else if(current_material==2){
      type = "Tile";
    }
    else if (current_material==3){
      type = "Metal";
    }
    return type;
}
}
