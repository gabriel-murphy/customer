import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-signature-confirmed',
  templateUrl: './signature-confirmed.component.html',
  styleUrls: ['./signature-confirmed.component.scss']
})
export class SignatureConfirmedComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
